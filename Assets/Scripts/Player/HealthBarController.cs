using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarController : MonoBehaviour
{
    public Image HealthBar;
    public float currentHealth;
    private float maxHealth;
    // Start is called before the first frame update
    void Start()
    {
        maxHealth = 100;
    }

    // Update is called once per frame
    void Update()
    {
        currentHealth = GameManager.Instance.Hp;
        HealthBar.fillAmount = currentHealth / maxHealth;
    }
}
