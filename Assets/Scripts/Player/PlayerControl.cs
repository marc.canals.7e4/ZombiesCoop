using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using Cinemachine;
using Unity.Netcode.Samples;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(NetworkObject))]
public class PlayerControl : NetworkBehaviour
{
    [SerializeField] private float walkSpeed = 3.5f;
    [SerializeField]
    public Inventory InventoryInfo;
    [SerializeField] private float runSpeed = 7f;

    [SerializeField] private float runSpeedOffset = 2.0f;
    [SerializeField] private int gunIndex;
    [SerializeField] private float rotationSpeed = 200f;
    [SerializeField] private Vector2 defaultInitialPositionOnPlane;
    private CharacterController characterController;
    [SerializeField] public gunController gunController;
    [SerializeField] public itemController itemController;
    [SerializeField] private Vector3 m_Input;
    [SerializeField] private Vector3 m_MoveDir, movePlayer, camForward, camRight;
    PlayerInputHandler m_InputHandler;
    public Animator playerHandsAnimRifle, playerAnimator;
    public RuntimeAnimatorController animatorPistol, animatorRifle;
    Quaternion m_CamRot, m_CharRot;
    bool clampVerticalRotation = true;
    [SerializeField] float m_CameraVerticalAngle = 0f;
    [SerializeField] public int hp;
    [SerializeField] float m_CameraHorizontalAngle = 0f;
    [SerializeField] float m_JumpSpeed;
    [SerializeField] float m_GravityMultiplier;
    [SerializeField] public Transform playerCameraPos;
    [SerializeField] public Rigidbody rigidBody;
    [SerializeField] public GameObject hands, realGun;
    [SerializeField] public GameObject body, gun;
    [SerializeField] bool m_IsWalking, animJump, animWalk;
    [SerializeField] bool m_IsJumping;
    [SerializeField] bool m_Jump;
    [SerializeField] bool m_PrevGround;
    [SerializeField] public bool isHosting;
    [SerializeField]
    private NetworkVariable<Vector3> networkPositionDirection = new NetworkVariable<Vector3>();
    [SerializeField]
    private NetworkVariable<Vector3> networkPositionFakeGun = new NetworkVariable<Vector3>();
    [SerializeField]
    private NetworkVariable<Vector3> networkRotationFakeGun = new NetworkVariable<Vector3>();
    [SerializeField]
    private NetworkVariable<Vector3> networkRotationDirection = new NetworkVariable<Vector3>();
    [SerializeField]
    private NetworkVariable<bool> networkJumpState = new NetworkVariable<bool>();
    [SerializeField]
    private NetworkVariable<bool> networkWalkState = new NetworkVariable<bool>();

    private void Awake()
    {
        if(GameManager.Instance.LevelComplete == true && SceneManager.GetActiveScene().name == "LobbyScene") { gameObject.transform.position = new Vector3(0, 0, 0); }
        hp = 100;
        GameManager.Instance.Hp = hp;
        playerAnimator = GetComponent<Animator>();
        m_InputHandler = GetComponent<PlayerInputHandler>();
        rigidBody = GetComponent<Rigidbody>();
        characterController = GetComponent<CharacterController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (IsClient && IsOwner)
        {
            gun.GetComponent<MeshRenderer>().enabled = false;
            body.GetComponent<SkinnedMeshRenderer>().enabled = false;


        }
        else {
            hands.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
            realGun.GetComponent<MeshRenderer>().enabled = false;
        }
        rotateGun();
    }


    // Update is called once per frame
    void Update()
    {
        if (IsClient && IsOwner)
        {
            if (PlayerCameraFollow.Instance != null)
            {
                PlayerCameraFollow.Instance.FollowPlayer(playerCameraPos);
                RotateView();
            }

            // the jump state needs to read here to make sure it is not missed
            if (!m_Jump)
            {
                m_Jump = Input.GetButtonDown("Jump");

            }



            if (characterController.isGrounded && !m_PrevGround)
            {
                m_MoveDir.y = 0f;
                m_IsJumping = false;
                animJump = false;
                playerAnimator.SetBool("IsJumping", false);
            }
            if (!characterController.isGrounded && !m_Jump && m_PrevGround)
            {
                m_PrevGround = characterController.isGrounded;
            }

            m_MoveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
            if (m_MoveDir != Vector3.zero)
            {
                playerAnimator.SetBool("IsWalking", true);
                animWalk = true;
            }
            else
            {
                playerAnimator.SetBool("IsWalking", false);
                animWalk = false;
            }
            m_Input = m_MoveDir;
            m_Input = Vector3.ClampMagnitude(m_Input, 1);
            movePlayer = m_Input.x * camRight + m_Input.z * camForward;

            float speed;
            ClientInput(out speed);

            if (characterController.isGrounded)
            {
                if (m_Jump)
                {
                    playerAnimator.SetBool("IsJumping", true);
                    animJump = true;
                    movePlayer.y = Mathf.Lerp(transform.position.y, m_JumpSpeed, 1);
                    m_Jump = false;
                    m_IsJumping = true;
                }
            }
            else
            {
                movePlayer += Physics.gravity * m_GravityMultiplier * Time.deltaTime;
            }
            characterController.Move(new Vector3(movePlayer.x * speed, movePlayer.y, movePlayer.z * speed) * Time.deltaTime);
            UpdateClientPositionServerRpc(this.transform.position, this.transform.localEulerAngles, animJump, animWalk);

            //this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        if (IsHost)
        {
            isHosting = true;
            UpdateClientPositionClientRpc();
        }

        // Inventory Switching 

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            gunController.loadData(0);
            playerHandsAnimRifle.runtimeAnimatorController = animatorRifle;
            rotateGun();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            gunController.loadData(1);
            playerHandsAnimRifle.runtimeAnimatorController = animatorPistol;
            rotateGun();
        }

        if (Input.GetKeyDown(KeyCode.Alpha3)) { itemController.loadConsumable(0);
            playerHandsAnimRifle.runtimeAnimatorController = animatorPistol;
            rotateItem(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4)) {itemController.loadConsumable(1);
            playerHandsAnimRifle.runtimeAnimatorController = animatorPistol;
            rotateItem(1);
        }



    }

    public void rotateItem(int index)
    {
       if (InventoryInfo.consumablesOnInventory[index].name == "Medic Kit")
        {
            realGun.transform.localPosition = new Vector3(0.0109999999f, 0.105999999f, 0.123999998f);
            realGun.transform.localEulerAngles = new Vector3(76.0000153f, 1.99990988f, 113.041397f);
            realGun.transform.localScale = new Vector3(4.62349987f  , 4.62349987f, 4.62349987f);
        }
        else
        {
            realGun.transform.localPosition = new Vector3(-0.0700000003f, 0.123000003f, 0);
            realGun.transform.localEulerAngles = new Vector3(8.41070652f, 262.241364f, 188.377106f);
            realGun.transform.localScale = new Vector3(0.0894980878f, 0.0894980878f, 0.0894980878f);
        }
    }
    public void rotateGun()
    {
        if (playerHandsAnimRifle.runtimeAnimatorController == animatorRifle)
        {
            if (gunController.bulletsPerTap == 1)
            {
                realGun.transform.localScale = new Vector3(1f,1f,1f);
                gunIndex = 0;
                realGun.transform.localEulerAngles = new Vector3(350.9f, 340.2f, 105.5f);
                realGun.transform.localPosition = new Vector3(-0.028f, 0.026f, 0.033f);
                gunController.fakeGun.transform.localPosition = new Vector3(0.022f, -0.03f, 0.058f);
                gunController.fakeGun.transform.localEulerAngles = new Vector3(331.6f, 115.9f, 223f);
                gunController.muzzleFlash.gameObject.transform.localRotation = new Quaternion(0,-90,0,0);
            }
            else
            {
                gunIndex = 0;
                realGun.transform.localScale = new Vector3(1f, 1f, 1f);
                realGun.transform.localEulerAngles = new Vector3(163.4f, 68.6f, 93.4f);
                realGun.transform.localPosition = new Vector3(0.02f, -0.12f, 0.033f);
                gunController.muzzleFlash.gameObject.transform.localRotation = new Quaternion(0, 90, 0, 0);
                gunController.fakeGun.transform.localPosition = new Vector3(0.064f, 0.004f, 0.068f);
                gunController.fakeGun.transform.localEulerAngles = new Vector3(27.07f, 162.14f, 41.07f);


            }
        }
        else {
            realGun.transform.localScale = new Vector3(1f, 1f, 1f);
            gunIndex = 1;
            realGun.transform.localEulerAngles = new Vector3(76f, 2f, 10.7f);
            realGun.transform.localPosition = new Vector3(-0.007f, -0.019f, -0.05f);
            gunController.muzzleFlash.gameObject.transform.localRotation = new Quaternion(0, 0, 0, 0);
            gunController.fakeGun.transform.localPosition = new Vector3(-0.06f, 0.115f, 0.048f);
            gunController.fakeGun.transform.localEulerAngles = new Vector3(303.75f, 263.75f, 26);
        }
        if (GameManager.Instance.Player != null)
        {
            if (isHosting) { UpdateGunsClientRPC(GameManager.Instance.Player.GetComponent<NetworkObject>().OwnerClientId, gunIndex); }
            if (!isHosting) { NotifyClientChangedGunServerRPC(GameManager.Instance.Player.GetComponent<NetworkObject>().OwnerClientId, gunController.fakeGun.transform.localPosition, gunController.fakeGun.transform.localEulerAngles, gunIndex);}
           
            
        }
    }

    private void RotateView()
    {
        camForward = PlayerCameraFollow.Instance.cinemachineVirtualCamera.transform.forward;
        camRight = PlayerCameraFollow.Instance.cinemachineVirtualCamera.transform.right;
        m_CameraHorizontalAngle += m_InputHandler.GetLookInputsHorizontal() * rotationSpeed;
        m_CameraVerticalAngle += m_InputHandler.GetLookInputsVertical() * rotationSpeed;
        //m_CameraVerticalAngle = Mathf.Clamp(-m_CameraVerticalAngle, -20, 20);
        //hands.transform.localEulerAngles = new Vector3(m_CameraVerticalAngle, 0, 0);
        this.transform.localEulerAngles = new Vector3(0, m_CameraHorizontalAngle, 0);
    }
    private void ClientInput(out float speed)
    {
        bool wasWalking = m_IsWalking;
        m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
        if (!m_IsWalking)
        {
            playerHandsAnimRifle.SetBool("Running", true);
            gunController.canAim = false;
            gunController.canShoot = false;
        }
        else {
            playerHandsAnimRifle.SetBool("Running", false);
            gunController.canAim = true;
            gunController.canShoot = true;
        }
        speed = m_IsWalking ? walkSpeed : runSpeed;
    }
   
    [ServerRpc]
    public void UpdateClientPositionServerRpc(Vector3 newPosition, Vector3 rotation, bool jumpState, bool walkState)
    {
        networkPositionDirection.Value = newPosition;
        networkRotationDirection.Value = rotation;
        networkJumpState.Value = jumpState;
        networkWalkState.Value = walkState;
        this.transform.position = networkPositionDirection.Value;
        this.transform.localEulerAngles = networkRotationDirection.Value;
    }

    [ClientRpc]
    public void UpdateClientPositionClientRpc()
    {
        for (int i = 0; i < GameManager.Instance.Players.Length; i++) {
            if (GameManager.Instance.Player != GameManager.Instance.Players[i]) {
                GameManager.Instance.Players[i].GetComponent<Transform>().position = GameManager.Instance.Players[i].GetComponent<PlayerControl>().networkPositionDirection.Value;
                GameManager.Instance.Players[i].GetComponent<Transform>().localEulerAngles = GameManager.Instance.Players[i].GetComponent<PlayerControl>().networkRotationDirection.Value;
                bool animJumpState = GameManager.Instance.Players[i].GetComponent<PlayerControl>().networkJumpState.Value;
                bool animWalkState = GameManager.Instance.Players[i].GetComponent<PlayerControl>().networkWalkState.Value;
                GameManager.Instance.Players[i].GetComponent<Animator>().SetBool("IsJumping", animJumpState);
                GameManager.Instance.Players[i].GetComponent<Animator>().SetBool("IsWalking", animWalkState);
            }
        }
    }
    //Gun showing
    [ServerRpc(RequireOwnership = false)]
    public void NotifyClientChangedGunServerRPC(ulong ownerClientid, Vector3 fakeGunPos, Vector3 fakeGunRot, int indexGun) {
        Debug.Log("Update client gun request: id - " + ownerClientid);
        networkPositionFakeGun.Value = fakeGunPos;
        networkRotationFakeGun.Value = fakeGunRot;
        GameManager.Instance.Players[ownerClientid].GetComponentInChildren<gunController>().fakeGun.transform.localPosition = GameManager.Instance.Players[ownerClientid].GetComponentInChildren<PlayerControl>().networkPositionFakeGun.Value;
        GameManager.Instance.Players[ownerClientid].GetComponentInChildren<gunController>().fakeGun.transform.localEulerAngles = GameManager.Instance.Players[ownerClientid].GetComponentInChildren<PlayerControl>().networkRotationFakeGun.Value;
        GameManager.Instance.Players[ownerClientid].GetComponentInChildren<gunController>().fakeGun.GetComponent<MeshFilter>().mesh = GameManager.Instance.Players[ownerClientid].GetComponentInChildren<gunController>().InventoryInfo.gunsOnInventory[indexGun].gunMesh;
        GameManager.Instance.Players[ownerClientid].GetComponentInChildren<gunController>().fakeGun.GetComponent<MeshRenderer>().material = GameManager.Instance.Players[ownerClientid].GetComponentInChildren<gunController>().InventoryInfo.gunsOnInventory[indexGun].gunMeshMaterial;
        //GameManager.Instance.Players[i].GetComponent<gunController>().fakeGun.GetComponent<MeshFilter>().mesh = GameManager.Instance.Players[i].GetComponent<gunController>().gunPlaceHolder.GetComponent<MeshFilter>().mesh;
        //GameManager.Instance.Players[i].GetComponent<gunController>().fakeGun.GetComponent<MeshRenderer>().material = GameManager.Instance.Players[i].GetComponent<gunController>().gunPlaceHolder.GetComponent<MeshRenderer>().material;
    }

    [ClientRpc]
    public void UpdateGunsClientRPC(ulong idPlayer, int indexGun)
    {
        for (int i = 0; i < GameManager.Instance.Players.Length; i++) {
            if (GameManager.Instance.Players[i].GetComponent<NetworkObject>().OwnerClientId  == idPlayer) {
                GameManager.Instance.Players[i].GetComponentInChildren<gunController>().fakeGun.transform.localPosition = GameManager.Instance.Players[i].GetComponentInChildren<PlayerControl>().networkPositionFakeGun.Value;
                GameManager.Instance.Players[i].GetComponentInChildren<gunController>().fakeGun.transform.localEulerAngles = GameManager.Instance.Players[i].GetComponentInChildren<PlayerControl>().networkRotationFakeGun.Value;
                GameManager.Instance.Players[i].GetComponentInChildren<gunController>().fakeGun.GetComponent<MeshFilter>().mesh = GameManager.Instance.Players[i].GetComponentInChildren<gunController>().InventoryInfo.gunsOnInventory[indexGun].gunMesh;
                GameManager.Instance.Players[i].GetComponentInChildren<gunController>().fakeGun.GetComponent<MeshRenderer>().material = GameManager.Instance.Players[i].GetComponentInChildren<gunController>().InventoryInfo.gunsOnInventory[indexGun].gunMeshMaterial;
            }
        }
        //GameManager.Instance.Players[idPlayer].GetComponentInChildren<gunController>().fakeGun.transform.localPosition = GameManager.Instance.Players[idPlayer].GetComponentInChildren<PlayerControl>().networkPositionFakeGun.Value;
        //GameManager.Instance.Players[idPlayer].GetComponentInChildren<gunController>().fakeGun.transform.localEulerAngles = GameManager.Instance.Players[idPlayer].GetComponentInChildren<PlayerControl>().networkRotationFakeGun.Value;
        //GameManager.Instance.Players[idPlayer].GetComponentInChildren<gunController>().fakeGun.GetComponent<MeshFilter>().mesh = GameManager.Instance.Players[idPlayer].GetComponentInChildren<gunController>().InventoryInfo.gunsOnInventory[indexGun].gunMesh;
        //GameManager.Instance.Players[idPlayer].GetComponentInChildren<gunController>().fakeGun.GetComponent<MeshRenderer>().material = GameManager.Instance.Players[idPlayer].GetComponentInChildren<gunController>().InventoryInfo.gunsOnInventory[indexGun].gunMeshMaterial;
    }
}
