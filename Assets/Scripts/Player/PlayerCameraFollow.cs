using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Cinemachine;

public class PlayerCameraFollow : MonoBehaviour
{
    
    private static PlayerCameraFollow _instance;
    public static PlayerCameraFollow Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this);
    }
    
    public CinemachineVirtualCamera cinemachineVirtualCamera;

    private void Start()
    {
        GameManager.Instance.camera = this.gameObject;
        cinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
    }

    public void FollowPlayer(Transform _transform)
    {
        // not all scenes have a cinemachine virtual camera so return in that's the case
        if (cinemachineVirtualCamera == null) Debug.Log("Not found camera");

        cinemachineVirtualCamera.Follow = _transform;
        cinemachineVirtualCamera.LookAt = _transform;
    }
}
