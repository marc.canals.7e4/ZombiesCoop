﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatePatrolling : MonoBehaviour
{
    public GameObject[] WayPoints; //Array que almacena las posiciones a recorrer //esto cuando se inicia? desde el inspector  como dato a veces no le gusta que se inicialize solo por ahi

    public ControllerVision controllerVision;

    private StateMachine stateMachine;
    private ControllerNavMesh controllerNavMesh;

    private int nextWayPoint; // Referencia a la posici�n del array 'WayPoint'

    void Start()
    {
        stateMachine = GetComponent<StateMachine>();
        controllerNavMesh = GetComponent<ControllerNavMesh>();
        WayPoints = GameObject.FindGameObjectsWithTag("Waypoints");
        VerifyWayPointDestinity();
    }

    private void OnEnable()
    {
        if (GameObject.FindGameObjectsWithTag("Waypoints") != null)
        {
            WayPoints = GameObject.FindGameObjectsWithTag("Waypoints");
        }
    }

    void Update()
    {
        // Demon can see the player then:
        if (controllerVision.ZombieSeePlayer() )
        {
            controllerNavMesh.playerPos = controllerVision.playerPos;
            stateMachine.ActivateState(stateMachine.stateFollowPlayer);
            return; // Si esta condici�n se cumple, no continuara ejecutando la siguiente parte del c�digo.
        }        

        // Demon only is patrolling 
        if (controllerNavMesh.ZombieArriveToPos())
        {
            VerifyWayPointDestinity();
        }
    }

    void VerifyWayPointDestinity()
    {
        if (WayPoints == null) { return; }

        Debug.LogWarning("Next waypoints: " + nextWayPoint);
        Debug.LogWarning("Zombie destination: " + controllerNavMesh.navMeshAgent.destination);

        nextWayPoint = Random.Range(0, WayPoints.Length-1);
        controllerNavMesh.ZombieMovementnavMesh(WayPoints[nextWayPoint].transform.position); 
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && enabled)
        {
            stateMachine.ActivateState(stateMachine.stateAlert);
        }
    }
}
