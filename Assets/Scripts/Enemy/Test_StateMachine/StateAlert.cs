﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAlert : MonoBehaviour
{
    public float speedToRotateSearch = 120f;
    public float timeToLook = 4f;
    public ControllerVision controllerVision;


    private StateMachine stateMachine;
    private ControllerNavMesh controllerNavMesh;
    private float timeLookingFor;

    private void Awake()
    {
        controllerNavMesh = GetComponent<ControllerNavMesh>();
        stateMachine = GetComponent<StateMachine>();
        //controllerVision = GetComponent<ControllerVision>();
    }

    void Start()
    {

    }

    void Update()
    {
        transform.Rotate(0f, speedToRotateSearch * Time.deltaTime, 0f);
        timeLookingFor += Time.deltaTime;
        if (timeLookingFor >= timeToLook)
        {
            stateMachine.ActivateState(stateMachine.statePatrolling);
            return;
        }

        if (controllerVision.ZombieSeePlayer() && !stateMachine.stateFollowPlayer.isActiveAndEnabled)
        {
            //controllerNavMesh.playerPos = hit.transform;
            stateMachine.ActivateState(stateMachine.stateFollowPlayer);
            return; // Si esta condici�n se cumple, no continuara ejecutando la siguiente parte del c�digo. (Optimizar despu�s)
        }
    }

    private void OnEnable()
    {
        controllerNavMesh.ZombieStopNavMesh();
        timeLookingFor = 0f;
    }
}
