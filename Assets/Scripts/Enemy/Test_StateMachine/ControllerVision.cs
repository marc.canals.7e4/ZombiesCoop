﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerVision : MonoBehaviour
{
    [Header("Controllers Parameters")]
    //public SphereCollider sphereCollider;
    [SerializeField] StateMachine stateMachine;

    [HideInInspector]
    public Transform playerPos;
    bool seePlayer = false;
    //public Transform eyesDemon;
    //public float rayCastVisionLenght = 30f; // Longitud del rayo de los ojos 
    //public Vector3 offset;

    private ControllerNavMesh controllerNavMesh;

    private void Awake()
    {
        //sphereCollider = GetComponent<SphereCollider>();
        //controllerNavMesh = GetComponent<ControllerNavMesh>();
    }

    void Start()
    {
        playerPos = GetComponent<Transform>();
        stateMachine = GetComponent<StateMachine>();
    }

    public bool ZombieSeePlayer()
    {
        //Debug.Log("Miro al weon: " + seePlayer);
        /*if (sphereCollider.isTrigger)
        {
            Debug.Log("TE VEO");
            playerPos = sphereCollider.gameObject.transform;
            seePlayer = true;
        }
        else
        {
            seePlayer = false;
        }*/
        
        return seePlayer;
        /*  Vector3 _direction;

          if (lookAtPlayer)
          {
              _direction = (controllerNavMesh.playerPos.position + offset) - eyesDemon.position; // Direcci�n de los ojos siempre vean al jugador fijamente y no a los pies.
          }
          else
          {
              _direction = eyesDemon.forward;

          }

          Debug.DrawRay(eyesDemon.position, _direction, Color.red);
         // Debug.Log("ZOmbie veiw _dir:" + _direction);


          return Physics.Raycast(eyesDemon.position, _direction, out hit, rayCastVisionLenght) && hit.collider.CompareTag("Player");
        */
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Entra");
        if (other.gameObject.CompareTag("Player")) seePlayer = true;

    }

    private void OnTriggerStay(Collider other)
    {
        
        if (other.gameObject.CompareTag("Player"))
        {
            //Debug.Log("Mantiene" + playerPos);
            playerPos = other.gameObject.transform;
        }               
    }

    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("Sale");
        if (other.gameObject.CompareTag("Player")) seePlayer = false;
        //if (stateMachine.stateHit.isActiveAndEnabled) stateMachine.ActivateState(stateMachine.stateAlert);
    }
}
