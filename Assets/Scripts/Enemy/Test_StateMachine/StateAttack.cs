using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAttack : MonoBehaviour
{
    public ControllerVision controllerVision;

    StateMachine stateMachine;
    ControllerNavMesh controllerNavMesh;
    AttackCollider attackcolliderscript;
    GameObject attackcollider;
    //Animator zombieAnimation;

    void Start()
    {
        attackcollider = GameObject.FindWithTag("attackCollider");
        controllerNavMesh = GetComponent<ControllerNavMesh>();
        stateMachine = GetComponent<StateMachine>();
        Debug.Log("ZOmbie:  StateAttack");
    }

    void Update()    
    {
        if (!controllerVision.ZombieSeePlayer() || !controllerNavMesh.ZombieArriveToPos())
        {
            stateMachine.ActivateState(stateMachine.stateAlert);
            return;
        }
        else
        {
            controllerNavMesh.playerPos = controllerVision.playerPos;
            controllerNavMesh.ZombieMovementnavMesh();
        }
    }

    private void OnEnable()
    {
        
    }

    private void OnCollisionExit(Collision collision)
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        for (int i = 0; i < GameManager.Instance.Players.Length; i++) {
            if (GameManager.Instance.Players[i] == collision.gameObject) {
                GameManager.Instance.Players[i].GetComponent<PlayerControl>().hp -= 1;
                GameManager.Instance.Hp = GameManager.Instance.Players[i].GetComponent<PlayerControl>().hp;
            }
        }
    }
    private void OnDisable()
    {
    }
}
