using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationZombieController : MonoBehaviour
{
    [Header("Controller Parameters")]
    [SerializeField] Animator zombieAnimation;
    public ZombieNetwork zombieNetwork;
    StateMachine stateMachine;
    
    void Start()
    {
        zombieNetwork = GetComponent<ZombieNetwork>();
        stateMachine = GetComponent<StateMachine>();
    }

    
    void Update()
    {
        VerifyStateForAnimation();
    }

    
    public void VerifyStateForAnimation()
    {

        if (stateMachine.stateFollowPlayer.isActiveAndEnabled)
        {
            zombieAnimation.ResetTrigger("TriggerAlert");
            zombieAnimation.ResetTrigger("TriggerClosePlayer");
            zombieAnimation.SetTrigger("TriggerMove");
            zombieNetwork.isAttacking = false;
            zombieNetwork.isAlert = false;
            zombieNetwork.isRunning = true;
        }
        else if (stateMachine.stateAlert.isActiveAndEnabled)
        {
            zombieAnimation.ResetTrigger("TriggerMove");
            zombieAnimation.ResetTrigger("TriggerClosePlayer");
            zombieAnimation.SetTrigger("TriggerAlert");
            zombieNetwork.isAttacking = false;
            zombieNetwork.isRunning = false;
            zombieNetwork.isAlert = true;
        }
        else if (stateMachine.statePatrolling.isActiveAndEnabled)
        {
            zombieAnimation.ResetTrigger("TriggerAlert");
            zombieAnimation.ResetTrigger("TriggerClosePlayer");
            zombieAnimation.SetTrigger("TriggerMove");
            zombieNetwork.isAlert = false;
            zombieNetwork.isAttacking = false;
            zombieNetwork.isRunning = true;
        }
        else if (stateMachine.stateAttack.isActiveAndEnabled)
        {
            zombieAnimation.ResetTrigger("TriggerAlert");
            zombieAnimation.ResetTrigger("TriggerClosePlayer");
            zombieAnimation.SetTrigger("TriggerClosePlayer");
            zombieNetwork.isAlert = false;
            zombieNetwork.isRunning = false;
            zombieNetwork.isAttacking = true;
        }
     
    }
}
