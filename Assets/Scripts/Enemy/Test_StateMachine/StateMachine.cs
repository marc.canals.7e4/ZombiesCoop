using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Animator))]
public class StateMachine : MonoBehaviour
{
    [Header("States parameters")]
    public MonoBehaviour stateAlert;
    public MonoBehaviour stateFollowPlayer;
    public MonoBehaviour statePatrolling;
    public MonoBehaviour stateAttack;
    public MonoBehaviour stateInitial;

    public MonoBehaviour stateActual;

    [HideInInspector]
    //public string animation;

    [Header("Controller Parameters")]
    [SerializeField] ParticleSystem blood;
    public ZombieNetwork zombieNetwork;
    ControllerNavMesh controllerNav;
    public ObjectPool objectPool;
    private Animator zombieAnimation;
    public int health = 5;
    public float cooldown = 5;
    float cooldownActual;


    void Start()
    {
        zombieNetwork = GetComponent<ZombieNetwork>();
        controllerNav = GetComponent<ControllerNavMesh>();
        cooldownActual = cooldown;
        //animation = "TriggerMove";
        zombieAnimation = GetComponent<Animator>();
        zombieNetwork.zombieHP = health;
        ActivateState(stateInitial);//
    }

    public void ActivateState(MonoBehaviour state)
    {
        if (health > 0)
        {
            if (stateActual != null) stateActual.enabled = false;
            stateActual = state;
            stateActual.enabled = true;
            //zombieAnimation.SetTrigger(animation);
        }
        else
        {
            if (stateActual != null) stateActual.enabled = false;
            stateActual.enabled = false;
            return;
        }
    }

    private void Update()
    {
        if (objectPool == null && GameObject.FindGameObjectWithTag("Pool") != null)
        {
            objectPool = GameObject.FindGameObjectWithTag("Pool").GetComponent<ObjectPool>();
        }
        CooldownForBackPool();
    }

    void CooldownForBackPool()
    {
        if (cooldownActual > 0 && health <= 0)
        {
            //Debug.LogWarning("Estoy entrando al cooldown we");
            cooldownActual -= Time.deltaTime;

        }
        else if (cooldownActual < 0)
        {
            objectPool.ReturnObject(this.gameObject);
        }
    }

    private void OnDestroy()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            Destroy(collision.gameObject);
            //HitAnimation();
            //ActivateState(stateHit);
            //Debug.Log("ESTOY COLSIONANDO PTM");         
            if (zombieNetwork.zombieHP > 0)
            {
                health -= GameManager.Instance.dmgBullet;
                zombieNetwork.zombieHP = health;
                if (!GameManager.Instance.Player.GetComponent<PlayerControl>().isHosting)
                {
                    zombieNetwork.UpdateHPZombieServerRPC(health);
                }
            }

            if (!blood.isPlaying)
            {
                blood.Play();
            }

            if (zombieNetwork.zombieHp.Value < 1)
            {
                controllerNav.ZombieStopNavMesh();
                controllerNav.navMeshAgent.SetDestination(new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z));

                zombieAnimation.ResetTrigger("TriggerAlert");
                zombieAnimation.ResetTrigger("TriggerMove");
                zombieAnimation.ResetTrigger("TriggerClosePlayer");
                zombieAnimation.SetTrigger("EnemyDead");
                zombieNetwork.isDead = true;
                zombieNetwork.zombieHP = health;
                if (!GameManager.Instance.Player.GetComponent<PlayerControl>().isHosting)
                {
                    zombieNetwork.UpdateHPZombieServerRPC(health);
                }
                if (health <= 0)
                {
                    Debug.LogWarning("El zombie ha muerto");
                    GameManager.Instance.skulls++;
                }
                return;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {

    }
}
