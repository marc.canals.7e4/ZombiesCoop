using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFollowPlayer : MonoBehaviour
{
    public ControllerVision controllerVision;

    private StateMachine stateMachine;
    private ControllerNavMesh controllerNavMesh;

    void Start()
    {
        controllerNavMesh = GetComponent<ControllerNavMesh>();
        //controllerVision = GetComponent<ControllerVision>();
        stateMachine = GetComponent<StateMachine>();
    }

    void Update()
    {
        //RaycastHit _hit;
        //Debug.LogWarning("Mientras lo sigo, he llegado? " + controllerNavMesh.ZombieArriveToPos());
        if (!controllerVision.ZombieSeePlayer())
        {
            stateMachine.ActivateState(stateMachine.stateAlert);
            return;
        }else if (controllerNavMesh.ZombieArriveToPos()) {
            //Debug.Log("Le parto la chucha a este we");
            //controllerNavMesh.ZombieStopNavMesh();
            stateMachine.ActivateState(stateMachine.stateAttack);//attack
            return;
        }
        controllerNavMesh.playerPos = controllerVision.playerPos;
        controllerNavMesh.ZombieMovementnavMesh();
    }

    private void OnEnable()
    {
        //stateMachine.animation = "TriggerMove";
        //controllerNavMesh.ZombieMovementnavMesh();
    }
}
