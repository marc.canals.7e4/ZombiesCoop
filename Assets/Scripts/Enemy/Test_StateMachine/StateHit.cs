using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateHit : MonoBehaviour
{
    [SerializeField] Animator zombieAnimation;

    public ControllerVision controllerVision;

    private StateMachine stateMachine;
    private ControllerNavMesh controllerNavMesh;

    void Start()
    {
        stateMachine = GetComponent<StateMachine>();
        controllerNavMesh = GetComponent<ControllerNavMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        if (zombieAnimation.GetCurrentAnimatorStateInfo(0).normalizedTime > 2.15f)
        {
            //Debug.Log("ENTROOOOOOOOOOOO AJHHH");
            controllerNavMesh.ZombieStopNavMesh();
            stateMachine.ActivateState(stateMachine.stateFollowPlayer);
        }
    }
}
