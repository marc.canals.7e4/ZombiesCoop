using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControllerNavMesh : MonoBehaviour
{
    [HideInInspector] 
    public Transform playerPos;
    public Vector3 destinationPoint;
    public NavMeshAgent navMeshAgent;

    //[Header("Controllers parameters")]

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void ZombieMovementnavMesh()
    {
        ZombieMovementnavMesh(playerPos.position);
    }

    
    public void ZombieMovementnavMesh(Vector3 pointToFollow)
    {
        navMeshAgent.destination = pointToFollow;
        navMeshAgent.isStopped = false;
    }

    public void ZombieStopNavMesh()
    {
        navMeshAgent.isStopped = true;
    }

    public bool ZombieArriveToPos()
    {
        //Debug.LogWarning("Distancia pendiente (remainingDistance): " + navMeshAgent.remainingDistance);
        //Debug.LogWarning("Distancia para detenerse" + navMeshAgent.stoppingDistance);
        //Debug.LogWarning("Hay distancia pendiente por recorrer? " + navMeshAgent.pathPending);

        return navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance && !navMeshAgent.pathPending; // calcula la distancia entre el jugador y el zombie
    }
}
