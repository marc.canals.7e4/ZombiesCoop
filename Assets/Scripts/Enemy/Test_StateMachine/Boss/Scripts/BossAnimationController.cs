using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimationController : MonoBehaviour
{
    [Header("Controller Parameters")]
    [SerializeField] Animator bossAnimation;
    public ZombieNetwork zombieNetwork;
    StateMachineBoss stateMachine;
    ControllerNavMesh controllerNav;

    void Start()
    {
        zombieNetwork = GetComponent<ZombieNetwork>();
        stateMachine = GetComponent<StateMachineBoss>();
    }

    private void LateUpdate()
    {
        VerifyStateForAnimation();
    }

    public void VerifyStateForAnimation()
    {
        if (stateMachine.stateFollowPlayer.isActiveAndEnabled)
        {
            bossAnimation.ResetTrigger("BossAttack");
            bossAnimation.ResetTrigger("BossPatrolling");
            bossAnimation.ResetTrigger("BossAlert");
            bossAnimation.SetTrigger("BossFollow");
        }
        else if (stateMachine.stateAlert.isActiveAndEnabled)
        {
            bossAnimation.ResetTrigger("BossFollow");
            bossAnimation.ResetTrigger("BossPatrolling");
            bossAnimation.SetTrigger("BossAlert");
        }
        else if (stateMachine.statePatrolling.isActiveAndEnabled)
        {
            bossAnimation.ResetTrigger("BossFollow");
            bossAnimation.ResetTrigger("BossAlert");
            bossAnimation.ResetTrigger("BossAttack");
            bossAnimation.SetTrigger("BossPatrolling");
        }
        else if (stateMachine.stateAttack.isActiveAndEnabled)
        {
            bossAnimation.ResetTrigger("BossFollow");
            bossAnimation.ResetTrigger("BossPatrolling");
            bossAnimation.SetTrigger("BossAttack");
        }
        Debug.Log("stateAlert " + (stateMachine.stateAlert.isActiveAndEnabled) + "\n" +
            "stateAttack" + stateMachine.stateAttack.isActiveAndEnabled + "\n" +
            "stateFollow" + stateMachine.stateFollowPlayer.isActiveAndEnabled + "\n" +
            "statePatrolling" + stateMachine.statePatrolling.isActiveAndEnabled);
    }
}