using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStateAttack : MonoBehaviour
{
    public BossControllerVision controllerVision;

    StateMachineBoss stateMachine;
    ControllerNavMesh controllerNavMesh;
    AttackCollider attackcolliderscript;
    GameObject attackcollider;
    //Animator zombieAnimation;

    void Start()
    {
        attackcollider = GameObject.FindWithTag("attackCollider");
        //zombieAnimation = GetComponent<Animator>();
        controllerNavMesh = GetComponent<ControllerNavMesh>();
        //controllerVision = GetComponent<ControllerVision>();
        stateMachine = GetComponent<StateMachineBoss>();
        //attackcolliderscript = attackcollider.GetComponent<AttackCollider>();
        Debug.Log("ZOmbie:  StateAttack");
    }

    void Update()
    {
        if (!controllerVision.ZombieSeePlayer() || !controllerNavMesh.ZombieArriveToPos())
        {
            stateMachine.ActivateState(stateMachine.stateAlert);
            return;
        }
        else
        {
            controllerNavMesh.playerPos = controllerVision.playerPos;
            controllerNavMesh.ZombieMovementnavMesh();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        for (int i = 0; i < GameManager.Instance.Players.Length; i++)
        {
            if (GameManager.Instance.Players[i] == collision.gameObject)
            {
                GameManager.Instance.Players[i].GetComponent<PlayerControl>().hp -= 15;
                GameManager.Instance.Hp = GameManager.Instance.Players[i].GetComponent<PlayerControl>().hp;
            }
        }
    }
}
