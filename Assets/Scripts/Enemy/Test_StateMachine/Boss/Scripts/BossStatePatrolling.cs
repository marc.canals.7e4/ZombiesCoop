﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStatePatrolling : MonoBehaviour
{
    public GameObject[] WayPoints;

    public BossControllerVision controllerVision;

    private StateMachineBoss stateMachine;
    private ControllerNavMesh controllerNavMesh;

    private int nextWayPoint; 

    void Start()
    {
        stateMachine = GetComponent<StateMachineBoss>();
        controllerNavMesh = GetComponent<ControllerNavMesh>();
        WayPoints = GameObject.FindGameObjectsWithTag("Waypoints");
        VerifyWayPointDestinity();
    }

    private void OnEnable()
    {
        if (GameObject.FindGameObjectsWithTag("Waypoints") != null)
            WayPoints = GameObject.FindGameObjectsWithTag("Waypoints");
    }

    void Update()
    {        
        if (controllerVision.ZombieSeePlayer())
        {
            controllerNavMesh.playerPos = controllerVision.playerPos;
            stateMachine.ActivateState(stateMachine.stateFollowPlayer);
            return; 
        }

        if (controllerNavMesh.ZombieArriveToPos())
        {
            VerifyWayPointDestinity();
        }
    }

    void VerifyWayPointDestinity()
    {
        if (WayPoints == null) { return; }
        nextWayPoint = Random.Range(0, WayPoints.Length);
        controllerNavMesh.ZombieMovementnavMesh(WayPoints[nextWayPoint].transform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && enabled)
        {
            stateMachine.ActivateState(stateMachine.stateAlert);
        }
    }
}
