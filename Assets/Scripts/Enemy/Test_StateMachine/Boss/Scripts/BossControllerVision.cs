﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossControllerVision : MonoBehaviour
{
    [Header("Controllers Parameters")]
    //public SphereCollider sphereCollider;
    [SerializeField] StateMachineBoss stateMachine;

    [HideInInspector]
    public Transform playerPos;
    bool seePlayer = false;
    //public Transform eyesDemon;
    //public float rayCastVisionLenght = 30f; // Longitud del rayo de los ojos 
    //public Vector3 offset;

    private ControllerNavMesh controllerNavMesh;

    private void Awake()
    {
        //sphereCollider = GetComponent<SphereCollider>();
        //controllerNavMesh = GetComponent<ControllerNavMesh>();
    }

    void Start()
    {
        playerPos = GetComponent<Transform>();
        stateMachine = GetComponent<StateMachineBoss>();
    }

    public bool ZombieSeePlayer()
    {
        return seePlayer;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) seePlayer = true;

    }

    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            //Debug.Log("Mantiene" + playerPos);
            playerPos = other.gameObject.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("Sale");
        if (other.gameObject.CompareTag("Player")) seePlayer = false;
        //if (stateMachine.stateHit.isActiveAndEnabled) stateMachine.ActivateState(stateMachine.stateAlert);
    }
}
