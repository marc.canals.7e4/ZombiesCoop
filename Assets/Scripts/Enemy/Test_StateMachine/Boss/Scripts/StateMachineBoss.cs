using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachineBoss : MonoBehaviour
{
    [Header("States parameters")]
    public MonoBehaviour stateAlert;
    public MonoBehaviour stateFollowPlayer;
    public MonoBehaviour statePatrolling;
    public MonoBehaviour stateAttack;
    public MonoBehaviour stateInitial;

    public MonoBehaviour stateActual;

    [HideInInspector]
    //public string animation;

    [Header("Controller Parameters")]
    [SerializeField] ParticleSystem blood;
    public ZombieNetwork zombieNetwork;
    ControllerNavMesh controllerNav;
    public Animator bossAnimation;
    public int health = 5;
    public float cooldown = 5;
    float cooldownActual;


    void Start()
    {
        //zombieNetwork = GetComponent<ZombieNetwork>();
        controllerNav = GetComponent<ControllerNavMesh>();
        cooldownActual = cooldown;
        //animation = "TriggerMove";
        bossAnimation = GetComponent<Animator>();
        //zombieNetwork.zombieHP = health;
        ActivateState(stateInitial);
    }

    public void ActivateState(MonoBehaviour state)
    {
        if (health > 0)
        {
            if (stateActual != null) stateActual.enabled = false;
            stateActual = state;
            stateActual.enabled = true;
        }
        else
        {
            if (stateActual != null) stateActual.enabled = false;
            stateActual.enabled = false;
            return;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            Destroy(collision.gameObject);
            if (health > 0)
            {
                health -= GameManager.Instance.dmgBullet;
                //zombieNetwork.zombieHP = health;
            }

            if (health < 1)
            {
                controllerNav.ZombieStopNavMesh();
                controllerNav.navMeshAgent.SetDestination(new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z));

                bossAnimation.ResetTrigger("BossAttack");
                bossAnimation.ResetTrigger("BossFollow");
                bossAnimation.ResetTrigger("BossPatrolling");
                bossAnimation.ResetTrigger("BossAlert");
                bossAnimation.SetTrigger("BossDie");
                if (health <= 0)
                {
                    Debug.LogWarning("El Boss ha muerto");
                    GameManager.Instance.skulls++;
                }
                return;
            }
        }
    }
}
