using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStateFollowPlayer : MonoBehaviour
{
    public BossControllerVision controllerVision;

    private StateMachineBoss stateMachine;
    private ControllerNavMesh controllerNavMesh;

    void Start()
    {
        controllerNavMesh = GetComponent<ControllerNavMesh>();
        stateMachine = GetComponent<StateMachineBoss>();
    }

    void Update()
    {
        if (!controllerVision.ZombieSeePlayer())
        {
            stateMachine.ActivateState(stateMachine.stateAlert);
            return;
        }
        else if (controllerNavMesh.ZombieArriveToPos())
        {
            stateMachine.ActivateState(stateMachine.stateAttack);//attack
            return;
        }
        controllerNavMesh.playerPos = controllerVision.playerPos;
        controllerNavMesh.ZombieMovementnavMesh();
    }
}
