using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateZombie
{
    //enum State { idle, running, attacking }  damaged,  dead,  searching(?)
    Nothing,
    Appearing,
    IdleState,
    ChangePhase12,
    ChaseState,
    ChangePhase23,
    AttackState,
    HitState,
    Dead
}

public class Zombie : MonoBehaviour
{
    //enum Input { playerSee, playerClose}  damage recived, sound heard(?)

    public StateZombie State = StateZombie.Nothing;
    public float MoveSpeed;
    private GameObject playerObject;
    Animator zombieAnimation;
    bool triggerVision;
    public int health;
    private int maxhealth;
    private int difHealth;
    public SphereCollider visionTrigger;
    public BoxCollider closeToPlayer;

    void Start()
    {
        visionTrigger = GetComponent<SphereCollider>();
        closeToPlayer = GetComponent<BoxCollider>();
        playerObject = GameObject.FindGameObjectWithTag("Player");
        zombieAnimation = GetComponent<Animator>();
        triggerVision = false;
        maxhealth = health;
        difHealth = health;
        State = StateZombie.Appearing;
    }

    void Update()
    {
        switch (State)
        {
            case StateZombie.Appearing:
                appearing();
                break;
            case StateZombie.IdleState:
                IdleState();
                break;
            case StateZombie.ChaseState:
                ChaseState();
                break;
            case StateZombie.AttackState:
                AttackState();
                break;
            case StateZombie.Dead:
                deathState();
                break;
        }

    }

   private void OnTriggerEnter(Collider other)
    {

        if (other.tag.Equals("Player"))
        {
            Debug.Log("Zombie: Food is close to me.");
            triggerVision = true;
        }
       }

    private void OnCollisionEnter(Collision collision)
    {

        /*if (other.tag.Equals("bullet"))
        {
            Debug.Log("HIT BY AMMO");
            health -= 1;
            Destroy(other);
        }*/
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Zombie: Food is getting away.");
        zombieAnimation.ResetTrigger("TriggerClosePlayer");

}

    public void appearing()
    {
        State = StateZombie.IdleState;
    }

    public void IdleState()
    {
        if (health < difHealth) State = StateZombie.HitState;
        if (visionTrigger) State = StateZombie.ChaseState;
    }

    public void ChaseState()
    {
        zombieAnimation.SetTrigger("TriggerMove");
        transform.position = Vector3.MoveTowards(transform.position, playerObject.transform.position, MoveSpeed);
        transform.LookAt(playerObject.transform);
        if (health < difHealth) State = StateZombie.HitState;
    }

    public void AttackState()
    {

    }

    public void HitState()
    {
        difHealth = health;
        zombieAnimation.SetTrigger("TriggerHit");
        zombieAnimation.ResetTrigger("TriggerHit");
        if (health <= 0) State = StateZombie.Dead;
    }

    public void deathState()
    {
        GameManager.Instance.skulls += 1;
        zombieAnimation.SetTrigger("EnemyDead");
        zombieAnimation.applyRootMotion = true;
        this.gameObject.GetComponent<Zombie>().enabled = false;
    }
    public void changeHitAnim() {
        zombieAnimation.SetBool("TriggerHit", false);
    }
}
