using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
public class ZombieSpawner : MonoBehaviour
{
    public ObjectPool zombiesPool;
    public GameObject[] spawnPoints;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Spawn", 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Spawn() {
      
            if (zombiesPool.pool.Count > 0 && GameManager.Instance.Player.GetComponent<PlayerControl>().isHosting)
            {
                int spawnPoint = Random.Range(0, spawnPoints.Length);
            Debug.Log("Spawning at: spawnpoint -> " + spawnPoint);
                zombiesPool.SpawnObject(spawnPoints[spawnPoint].transform.position, spawnPoints[spawnPoint].transform.rotation); 
            }
        
        
    }
}
