using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class ZombieNetwork : NetworkBehaviour
{
    //Network var
    public NetworkVariable<Vector3> zombiePos = new NetworkVariable<Vector3>(NetworkVariableReadPermission.Everyone);
    public NetworkVariable<Vector3> zombieRot = new NetworkVariable<Vector3>(NetworkVariableReadPermission.Everyone);
    public NetworkVariable<int> zombieHp = new NetworkVariable<int>(NetworkVariableReadPermission.Everyone);
    public NetworkVariable<bool> zombieRun = new NetworkVariable<bool>(NetworkVariableReadPermission.Everyone);
    public NetworkVariable<bool> zombieAlert = new NetworkVariable<bool>(NetworkVariableReadPermission.Everyone);
    public NetworkVariable<bool> zombieDead = new NetworkVariable<bool>(NetworkVariableReadPermission.Everyone);
    public NetworkVariable<bool> zombieAttack = new NetworkVariable<bool>(NetworkVariableReadPermission.Everyone);
    public bool isRunning, isAlert, isDead, isAttacking;
    public int zombieHP;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (GameManager.Instance.Player == null) return;
        if (GameManager.Instance.Player.GetComponent<NetworkObject>().IsOwner && !GameManager.Instance.Player.GetComponent<PlayerControl>().isHosting)
        {
            UpdateZombiesPositionAndRotationClientRpc();

        }
        else if(GameManager.Instance.Player.GetComponent<PlayerControl>().isHosting){
            UpdateHPZombieClientRPC(zombieHP);
            UpdateZombiesPositionAndRotationServerRpc(this.transform.position, this.transform.localEulerAngles, isRunning, isAlert, isAttacking, isDead, zombieHP);

        }

    }
    [ServerRpc(RequireOwnership = false)]
    public void UpdateZombiesPositionAndRotationServerRpc(Vector3 newPos, Vector3 newRot, bool isRunning, bool isAlert, bool isAttacking, bool isDead, int hp)
    {
        zombieRun.Value = isRunning;
        zombieAlert.Value = isAlert;
        zombieAttack.Value = isAttacking;
        zombieDead.Value = isDead;
        zombieHp.Value = hp; 
        //Zombie pos sync
        zombiePos.Value = newPos;
        zombieRot.Value = newRot;
        //this.transform.position = zombiePos.Value;
        //this.transform.localEulerAngles = zombieRot.Value;

    }
    [ServerRpc(RequireOwnership = false)]
    public void UpdateHPZombieServerRPC(int health) {
        zombieHP = health;
        this.zombieHp.Value = zombieHP;
        Debug.LogError("Client hit zombie");
        //Client hit zombie
    }

    [ClientRpc]
    public void UpdateHPZombieClientRPC(int health) {
        zombieHP = health;
        //Client hit zombie
    }

    [ClientRpc]
    public void UpdateZombiesPositionAndRotationClientRpc()
    {
        if (GameManager.Instance.zombies.Length == 0) { return; }
        this.GetComponent<Transform>().localPosition = this.zombiePos.Value;
        this.GetComponent<Transform>().localPosition = this.zombiePos.Value;
        this.zombieHP = this.zombieHp.Value;
        this.GetComponent<Transform>().localPosition = this.zombiePos.Value;

        //Zombie anim sync
        if (zombieRun.Value)
        {
            this.GetComponent<Animator>().SetTrigger("TriggerMove");
        }
        else
        {
            this.GetComponent<Animator>().ResetTrigger("TriggerMove");
        }

        if (zombieAlert.Value)
        {
            this.GetComponent<Animator>().SetTrigger("TriggerAlert");
        }
        else
        {
            this.GetComponent<Animator>().ResetTrigger("TriggerAlert");
        }

        if (zombieAttack.Value)
        {
            this.GetComponent<Animator>().SetTrigger("TriggerClosePlayer");
        }
        else
        {
            this.GetComponent<Animator>().ResetTrigger("TriggerClosePlayer");
        }
        if (zombieDead.Value)
        {
            this.GetComponent<Animator>().SetTrigger("EnemyDead");
        }
        else
        {
            this.GetComponent<Animator>().ResetTrigger("EnemyDead");
        }
        /*
        if (GameManager.Instance.zombies.Length == 0) { return; }
        for (int i = 0; i < GameManager.Instance.zombies.Length; i++) {
            GameManager.Instance.zombies[i].GetComponent<Transform>().localPosition = GameManager.Instance.zombies[i].GetComponent<ZombieNetwork>().zombiePos.Value;
            GameManager.Instance.zombies[i].GetComponent<Transform>().localEulerAngles = GameManager.Instance.zombies[i].GetComponent<ZombieNetwork>().zombieRot.Value;
            GameManager.Instance.zombies[i].GetComponent<ZombieNetwork>().zombieHP = GameManager.Instance.zombies[i].GetComponent<ZombieNetwork>().zombieHp.Value;
            //Zombie anim sync
            if (zombieRun.Value)
            {
                GameManager.Instance.zombies[i].GetComponent<Animator>().SetTrigger("TriggerMove");
            }
            else
            {
                GameManager.Instance.zombies[i].GetComponent<Animator>().ResetTrigger("TriggerMove");
            }

            if (zombieAlert.Value)
            {
                GameManager.Instance.zombies[i].GetComponent<Animator>().SetTrigger("TriggerAlert");
            }
            else
            {
                GameManager.Instance.zombies[i].GetComponent<Animator>().ResetTrigger("TriggerAlert");
            }

            if (zombieAttack.Value)
            {
                GameManager.Instance.zombies[i].GetComponent<Animator>().SetTrigger("TriggerClosePlayer");
            }
            else
            {
                GameManager.Instance.zombies[i].GetComponent<Animator>().ResetTrigger("TriggerClosePlayer");
            }
            if (zombieDead.Value)
            {
                GameManager.Instance.zombies[i].GetComponent<Animator>().SetTrigger("EnemyDead");
            }
            else
            {
                GameManager.Instance.zombies[i].GetComponent<Animator>().ResetTrigger("EnemyDead");
            }
        }
        */
    }
    
}
