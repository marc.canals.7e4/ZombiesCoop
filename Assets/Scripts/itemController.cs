using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class itemController : NetworkBehaviour
{
    [SerializeField]
    public Inventory InventoryInfo;

    public GameObject  itemInInventory;
    [SerializeField]

    
    

    bool usingItem, readyToUse;

    public PlayerInputHandler inputHandler;

    string effect;
    int effectValue, itemUses;
    float timeToUse;
    int index;
    float itemUsingTime = 0;
    // Start is called before the first frame update
    void Awake()
    {
     //   itemInInventory = this.gameObject;
     //   loadConsumable(2);
    }

    // Update is called once per frame
    void Update()
    {
        GameManager.Instance.itemUse = itemUsingTime / timeToUse;

        if (IsClient && IsOwner)
        {
            MyInput();


        }
    }

    public void loadConsumable(int index)
    {
        itemInInventory.GetComponent<MeshFilter>().mesh = InventoryInfo.consumablesOnInventory[index].consumableMesh;
        itemInInventory.GetComponent<MeshRenderer>().material = InventoryInfo.consumablesOnInventory[index].consumableMeshMaterial;
        name = InventoryInfo.consumablesOnInventory[index].name;
        effect = InventoryInfo.consumablesOnInventory[index].effect;
        effectValue = InventoryInfo.consumablesOnInventory[index].effectValue;
        timeToUse = InventoryInfo.consumablesOnInventory[index].timeToUse;
        itemUses = InventoryInfo.consumablesOnInventory[index].itemUses;
        index = index;
    }

    private void MyInput()
    {
        
        if (Input.GetKey(KeyCode.Mouse0))
        {
            itemUsingTime++;
            GameManager.Instance.Player.GetComponentInChildren<gunController>().canShoot = false;
            GameManager.Instance.usingConsumable = true;
            
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            GameManager.Instance.Player.GetComponentInChildren<gunController>().canShoot = true;
            GameManager.Instance.usingConsumable = false;
            itemUsingTime = 0;
        }
        if (itemUsingTime == timeToUse && timeToUse!=0)
        {

            GameManager.Instance.usingConsumable = false;
            itemUsingTime = 0;
                itemUses--;
                if (effect == "Health")
                {
                    GameManager.Instance.Hp += effectValue;
                }
            InventoryInfo.consumablesOnInventory[index] = null;
        }
    }
}
