using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "InventoryData", menuName = "ScriptableObjects/InventoryData")]
public class Inventory : ScriptableObject
{
    public List<Gun> gunsOnInventory;
    public List<Consumable> consumablesOnInventory;
    
}
