using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "GunData", menuName = "ScriptableObjects/GunData")]
public class Gun : ScriptableObject
{
    public string descName;
    public bool canShot;
    public Mesh gunMesh;
    public Material gunMeshMaterial;
    public Sprite icon;

    //In case of beeing shootable weapon:
    public float timeBetweenShots;
    public float shootForce;
    public float spread;
    public float reloadTime;
    public float timeBetweenShooting;
    public int magazineSize;
    public int bulletsPerTap;
    public int damagePerBullet;
    public float recoilForce;
    public bool allowButtonHold;
    public Vector3 shootPoint;

    //In case of beeing mele weapon:
    public int damagePerHit;
    public float timeBetweenHit;
    public float mass;
    public string ammoUsed;

}
