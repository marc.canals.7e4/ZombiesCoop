using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "ConsumableData", menuName = "ScriptableObjects/ConsumableData")]
public class Consumable : ScriptableObject
{
    public Mesh consumableMesh;
    public Material consumableMeshMaterial;
    public string name;
    public Sprite icon;
    public string effect;
    public int effectValue;
    public float timeToUse;
    public int itemUses;

}
