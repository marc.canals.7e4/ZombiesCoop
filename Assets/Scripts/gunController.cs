using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Netcode;

public class gunController : NetworkBehaviour
{
    [SerializeField]
    public Inventory InventoryInfo;
    [SerializeField]
    //bullet 
    public GameObject bullet, gunPlaceHolder, fakeGun, playerUI;
    [SerializeField]
    public Transform shootPoint;
    //bullet force
    public float shootForce, upwardForce;

    public Animator playerHandsAnim;
    [SerializeField]
    //Gun stats
    public float timeBetweenShooting, spread, reloadTime, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    public bool allowButtonHold, canShoot, canAim, m_Inspect, shootLoaded = false;

    public int bulletsLeft, bulletsShot, dmgPerBullet;
    private string ammoType;
    //Recoil
    public Rigidbody handsRb;
    public float recoilForce;

    //bools
    bool shooting, readyToShoot, reloading;

    //Reference
    public Camera fpsCam;

    //Graphics
    public GameObject muzzleFlash;
    //public TextMeshProUGUI ammunitionDisplay;

    //Sound 
    public AudioClip shootGun, shootShotgun, shootRifle, reloadingSoundPrimaryWeapon, reloadingSoundGun;
    public AudioSource audioSource;

    //bug fixing :D
    public bool allowInvoke = true;
    public PlayerInputHandler inputHandler;
    private void Awake()
    {
        gunPlaceHolder = this.gameObject;
        loadData(0);
        //make sure magazine is full
        
        readyToShoot = true;     
    }
    public void loadData(int index) 
    {
        gunPlaceHolder.GetComponent<MeshFilter>().mesh = InventoryInfo.gunsOnInventory[index].gunMesh;
        gunPlaceHolder.GetComponent<MeshRenderer>().material = InventoryInfo.gunsOnInventory[index].gunMeshMaterial;
        fakeGun.GetComponent<MeshFilter>().mesh = InventoryInfo.gunsOnInventory[index].gunMesh;
        fakeGun.GetComponent<MeshRenderer>().material = InventoryInfo.gunsOnInventory[index].gunMeshMaterial;
        GameManager.Instance.dmgBullet = InventoryInfo.gunsOnInventory[index].damagePerBullet;
        timeBetweenShooting = InventoryInfo.gunsOnInventory[index].timeBetweenShooting;
        shootForce = InventoryInfo.gunsOnInventory[index].shootForce;
        spread = InventoryInfo.gunsOnInventory[index].spread;
        reloadTime = InventoryInfo.gunsOnInventory[index].reloadTime;
        timeBetweenShots = InventoryInfo.gunsOnInventory[index].timeBetweenShots;
        magazineSize = InventoryInfo.gunsOnInventory[index].magazineSize;
        bulletsPerTap = InventoryInfo.gunsOnInventory[index].bulletsPerTap;
        dmgPerBullet = InventoryInfo.gunsOnInventory[index].damagePerBullet;
        recoilForce = InventoryInfo.gunsOnInventory[index].recoilForce;
        allowButtonHold = InventoryInfo.gunsOnInventory[index].allowButtonHold;
        shootPoint.localPosition = InventoryInfo.gunsOnInventory[index].shootPoint;
        ammoType = InventoryInfo.gunsOnInventory[index].ammoUsed;
    }
    private void Update()
    {
         //bulletsUsed = GameManager.Instance.ammoType; 

        if (IsClient && IsOwner)
        {
            MyInput();

            
        }

        if (ammoType == "shotgunAmmo") { bulletsLeft = GameManager.Instance.shotgunMag; }
        if (ammoType == "rifleAmmo") { bulletsLeft = GameManager.Instance.rifleMag; }
        if (ammoType == "pistolAmmo") { bulletsLeft = GameManager.Instance.pistolMag; }
    }
    private void MyInput()
    {
        //Check if allowed to hold down button and take corresponding input
        if (allowButtonHold) shooting = Input.GetKey(KeyCode.Mouse0);
        else shooting = Input.GetKeyDown(KeyCode.Mouse0);


        //AIM OF GUN
        if (inputHandler.GetAimInputHeld() && canAim) {
            playerHandsAnim.SetFloat("Aiming", 1);
            spread = spread / 2;
        }
        else
        {
            playerHandsAnim.SetFloat("Aiming", 0);
        }

        //INSPECT OF GUN
        if (!m_Inspect && canShoot)
        {
            m_Inspect = Input.GetButtonDown("Inspect");
            if (m_Inspect)
            {
                playerHandsAnim.SetTrigger("Inspect");
                canShoot = false;
                StartCoroutine(delayUntilNewInspect());
            }
        }

        //Reloading 
        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading) Reload();
        //Reload automatically when trying to shoot without ammo
        if (readyToShoot && shooting && !reloading && bulletsLeft <= 0) Reload();
        if (readyToShoot && !shootLoaded)
        {
            StartCoroutine(letShoot());
        }
        //Shooting
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0 && canShoot)
        {
            //Set bullets shot to 0
            bulletsShot = 0;

            Shoot();
            if (ammoType == "shotgunAmmo") {  audioSource.PlayOneShot(shootShotgun); }
        }
    }
    private IEnumerator delayUntilNewInspect()
    {
        yield return new WaitForSeconds(2f);
        playerHandsAnim.ResetTrigger("Inspect");
        m_Inspect = false;
        canShoot = true;
    }
    private IEnumerator letShoot() {
        readyToShoot = false;
        shootLoaded = true;
        yield return new WaitForSeconds(3f);
        readyToShoot = true;
    }

    private void Shoot()
    {
        playerHandsAnim.SetBool("isShooting", true);
        readyToShoot = false;
        fpsCam = GameObject.FindGameObjectWithTag("Camera").GetComponent<Camera>();
        //Find the exact hit position using a raycast
        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); //Just a ray through the middle of your current view
        RaycastHit hit;

        //check if ray hits something
        Vector3 targetPoint;
        if (Physics.Raycast(ray, out hit))
        {
            targetPoint = hit.point;
        }
        else
            targetPoint = ray.GetPoint(75); //Just a point far away from the player

        //Calculate direction from attackPoint to targetPoint
        Vector3 directionWithoutSpread = targetPoint - shootPoint.position;

        //Calculate spread
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        //Calculate new direction with spread
        Vector3 directionWithSpread = directionWithoutSpread + new Vector3(x, y, 0); //Just add spread to last direction

        //Instantiate bullet/projectile
        GameObject currentBullet = Instantiate(bullet, shootPoint.position, Quaternion.identity); //store instantiated bullet in currentBullet
        //currentBullet.GetComponent<NetworkObject>().Spawn();
        //Rotate bullet to shoot direction
        currentBullet.transform.forward = directionWithSpread.normalized;

        //Add forces to bullet
        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);
        currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.up * upwardForce, ForceMode.Impulse);

        //Instantiate muzzle flash, if you have one

        if (muzzleFlash != null) {
            muzzleFlash.GetComponent<ParticleSystem>().Play();
           // muzzleFlash.GetComponent<NetworkObject>().Spawn();
        }


        
        if (ammoType == "rifleAmmo") { GameManager.Instance.rifleMag--; audioSource.PlayOneShot(shootRifle); }
        if (ammoType == "pistolAmmo") {GameManager.Instance.pistolMag--; audioSource.PlayOneShot(shootGun); }
        if (ammoType == "shotgunAmmo") { GameManager.Instance.shotgunMag--; }
            bulletsShot++;

        //Invoke resetShot function (if not already invoked), with your timeBetweenShooting
        if (allowInvoke)
        {
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;

            //Add recoil to player (should only be called once)
            handsRb.AddForce(-directionWithSpread.normalized * recoilForce, ForceMode.Impulse);
        }

        //if more than one bulletsPerTap make sure to repeat shoot function
        if (bulletsShot < bulletsPerTap && bulletsLeft > 0)
            Invoke("Shoot", timeBetweenShots);
    }
    private void ResetShot()
    {
        //Allow shooting and invoking again
        playerHandsAnim.SetBool("isShooting", false);
        readyToShoot = true;
        allowInvoke = true;
    }

    private void Reload()
    {
        if (magazineSize == 15 && GameManager.Instance.pistolAmmo > 0 || magazineSize == 60 && (GameManager.Instance.shotgunAmmo/bulletsPerTap) >= 1 || magazineSize == 32 && GameManager.Instance.rifleAmmo > 0)
        {
            audioSource.PlayOneShot(reloadingSoundGun);
            audioSource.PlayOneShot(reloadingSoundPrimaryWeapon);
            playerHandsAnim.SetTrigger("isReloading");
            reloading = true;
            Invoke("ReloadFinished", reloadTime); //Invoke ReloadFinished function with your reloadTime as delay
        }
    }
    private void ReloadFinished()
    {
        //Fill magazine
        if (magazineSize == 15)
        {
            GameManager.Instance.pistolAmmo -= magazineSize - bulletsLeft;

            if (GameManager.Instance.pistolAmmo > (magazineSize-bulletsLeft))
            {
                GameManager.Instance.pistolMag = magazineSize;
            }
            else
            {
                GameManager.Instance.pistolMag += GameManager.Instance.pistolAmmo;
            }
        }

        if (magazineSize == 32)
        {
            GameManager.Instance.rifleAmmo -= magazineSize - bulletsLeft;

            if (GameManager.Instance.rifleAmmo > (magazineSize - bulletsLeft))
            {
                GameManager.Instance.rifleMag = magazineSize;
            }
            else
            {
                GameManager.Instance.rifleMag += GameManager.Instance.rifleAmmo;
            }
        }

        if (magazineSize == 60)
        {
            GameManager.Instance.shotgunAmmo -= magazineSize - bulletsLeft;

            if (GameManager.Instance.shotgunAmmo > (magazineSize - bulletsLeft))
            {
                GameManager.Instance.shotgunMag = magazineSize;
            }
            else
            {
                GameManager.Instance.shotgunMag += GameManager.Instance.shotgunAmmo;
            }
        }

        reloading = false;
    }
}

