using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GunInteraction : MonoBehaviour
{
    public Gun item;
    public TextMeshProUGUI text;
    private bool isInteractable;



    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        { isInteractable = true; }
        else { isInteractable = false; }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            text.gameObject.SetActive(true);


        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (isInteractable)
        {
            if (item.descName.Equals("Pistol")|| item.descName.Equals("Wrench"))
            {
                GameManager.Instance.Player.GetComponent<gunController>().InventoryInfo.gunsOnInventory[1] = item;
                GameManager.Instance.Player.GetComponent<gunController>().loadData(1);
                GameManager.Instance.Player.GetComponent<PlayerControl>().rotateGun();
            }
            else {
                GameManager.Instance.Player.GetComponentInChildren<gunController>().InventoryInfo.gunsOnInventory[0] = item;
                GameManager.Instance.Player.GetComponentInChildren<gunController>().loadData(0);
                GameManager.Instance.Player.GetComponent<PlayerControl>().rotateGun();

            }

            Destroy(this.gameObject);
        }


    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            text.gameObject.SetActive(false);
        }

    }
}
