using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ItemInteraction : MonoBehaviour
{
    public Consumable item;
    public TextMeshProUGUI text;
    private bool isInteractable;

   

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        { isInteractable = true; }
        else { isInteractable = false; }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            text.gameObject.SetActive(true);

        
        }
    }

    private void OnTriggerStay(Collider other)
    {
        bool municion=false;

        if (isInteractable)
        {
            if (gameObject.tag != "AmmoBox")
            {
                if (GameManager.Instance.Player.GetComponentInChildren<gunController>().InventoryInfo.consumablesOnInventory[0] != null)
                {
                    GameManager.Instance.Player.GetComponentInChildren<gunController>().InventoryInfo.consumablesOnInventory[1] = item;
                }
                else {
                    GameManager.Instance.Player.GetComponentInChildren<gunController>().InventoryInfo.consumablesOnInventory[0] = item;
                }

            }
            else {
                if (municion == false) {
                    if (item.effect == "Gun Ammo") { GameManager.Instance.pistolAmmo += item.effectValue; }
                    if (item.effect == "Shotgun Ammo") { GameManager.Instance.shotgunAmmo += item.effectValue; }
                    if (item.effect == "Rifle Ammo") { GameManager.Instance.rifleAmmo += item.effectValue; }
                    municion = true;
                } }

            Destroy(this.gameObject);
        }
           
        
    }

    private void OnTriggerExit(Collider other)
    {
        
            if (other.gameObject.CompareTag("Player"))
            {
                text.gameObject.SetActive(false);
            }
        
    }
}
