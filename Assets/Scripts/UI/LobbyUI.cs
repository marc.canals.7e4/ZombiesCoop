using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Netcode;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyUI : NetworkBehaviour
{
    public GameObject terminalUI;
    public TextMeshProUGUI skulls;
    public GameObject supportButton;
    [SerializeField]
    public GameObject menu, deadMenu;
    [SerializeField]
    public TextMeshProUGUI relayCodeText, tutorialText;
    [SerializeField]
    public TextMeshProUGUI ammunitionDisplay;
    [SerializeField]
    public gunController gunController;
    public Image InvIcon01,InvIcon02,InvIcon03,InvIcon04;
    [SerializeField]
    public List<GameObject> HpBars;
    private static LobbyUI _instance;
    public static LobbyUI Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this);
    }

    // Start is called before the first frame update
    void Start()
    {
            if (RelayManager.Instance.IsRelayEnabled)
            {
                relayCodeText.text = "Join code: " + RelayManager.Instance.relayHost.JoinCode;
            }
   
    }
    void Update() {

        skulls.text = GameManager.Instance.skulls+" Skulls";
        if (Input.GetKeyDown(KeyCode.Escape)) {
            menuStateChange();
        }

        for (int i = 0; i < GameManager.Instance.Players.Length; i++) {
            HpBars[i].GetComponent<Slider>().value = GameManager.Instance.Players[i].GetComponent<PlayerControl>().hp;
        }

        if (ammunitionDisplay != null)
            {
            if (GameManager.Instance.Player != null)
            {
                gunController = GameManager.Instance.Player.GetComponentInChildren<gunController>();



                if (gunController.InventoryInfo.gunsOnInventory[0] != null)
                {
                    InvIcon01.sprite = gunController.InventoryInfo.gunsOnInventory[0].icon;
                }
                if (gunController.InventoryInfo.gunsOnInventory[1] != null)
                {
                    InvIcon02.sprite = gunController.InventoryInfo.gunsOnInventory[1].icon;
                }
                if (gunController.InventoryInfo.consumablesOnInventory[0] != null)
                {
                    InvIcon03.sprite = gunController.InventoryInfo.consumablesOnInventory[0].icon;
                }
                if (gunController.InventoryInfo.consumablesOnInventory[1] != null)
                {
                    InvIcon04.sprite = gunController.InventoryInfo.consumablesOnInventory[1].icon;
                }

                if (gunController.magazineSize == 15) { ammunitionDisplay.SetText(GameManager.Instance.pistolMag / gunController.bulletsPerTap + " / " + GameManager.Instance.pistolAmmo / gunController.bulletsPerTap); }
                if (gunController.magazineSize == 60) { ammunitionDisplay.SetText(GameManager.Instance.shotgunMag / gunController.bulletsPerTap + " / " + GameManager.Instance.shotgunAmmo / gunController.bulletsPerTap); }
                if (gunController.magazineSize == 32) { ammunitionDisplay.SetText(GameManager.Instance.rifleMag / gunController.bulletsPerTap + " / " + GameManager.Instance.rifleAmmo / gunController.bulletsPerTap); }
            }
        }
        if (SceneManager.GetActiveScene().name == "GameScene") {
            relayCodeText.enabled = false;
            tutorialText.text = "Reach the cabin teleporters on the big city. Evade danger and kill as many zombies as possible.";
            
        }
        if (SceneManager.GetActiveScene().name == "LobbyScene")
        {
            tutorialText.text = "Use WASD to move, when all players are in game press [E] on the Computer. For more tutorials read the signs in the Lobby.";

        }

    }
    public void desactivateDeadUI() {
        deadMenu.SetActive(false);
        supportButton.SetActive(true);
    }
    public void menuStateChange() {
        if (menu.activeSelf) { 
            menu.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            menu.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
    public void continueToLobby() {
        if (IsHost)
        {
            NetworkManager.Singleton.SceneManager.LoadScene("LobbyScene", LoadSceneMode.Single);
            for (int i = 0; i < GameManager.Instance.Players.Length; i++) {
                GameManager.Instance.Players[i].transform.position = new Vector3(0, 10, i);
            }
            GameManager.Instance.OnSceneChanged();

        }
    }
    public void returnToLobby() {
        
        
        GameManager.Instance.gameNetPortal.GetComponent<GameNetPortal>().RequestDisconnect();
        //GameManager.Instance.gameNetPortal.GetComponent<GameNetPortal>().OnUserDisconnectRequested += NotifyAndDestroyPlayerIngameServerRPC();
        Destroy(GameManager.Instance.networkManager.gameObject);
        Destroy(GameManager.Instance.gameNetPortal.gameObject);
        Destroy(GameManager.Instance.camera.gameObject);
        Destroy(this.gameObject);
        Destroy(GameManager.Instance.gameObject);
    }
    public void activateHP(int index) {
        HpBars[index].SetActive(true);
        if (GameManager.Instance.Player.GetComponent<PlayerControl>().isHosting)
        {
            activateHPBarAndUpdateServerRPC(index);
        }
        else {
            activateHPBarAndUpdateClientRPC(index);
        }
        
    }
    [ServerRpc]
    public void NotifyAndDestroyPlayerIngameServerRPC() {
        Debug.Log("Player disconnected");
    }
    [ServerRpc]
    public void activateHPBarAndUpdateServerRPC(int index)
    {
        GameManager.Instance.ui.GetComponent<LobbyUI>().HpBars[index].SetActive(true);
        Debug.Log("Need to activate hp"+index);
    }

    [ClientRpc]
    public void activateHPBarAndUpdateClientRPC(int index)
    {
        GameManager.Instance.ui.GetComponent<LobbyUI>().HpBars[index].SetActive(true);
        Debug.Log("Need to activate hp" + index);
    }
    // Update is called once per frame
    void ExitUIButton()
    {
        Cursor.lockState = CursorLockMode.Locked;
        terminalUI.SetActive(true);
    }
}
