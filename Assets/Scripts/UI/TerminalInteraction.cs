using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TerminalInteraction : MonoBehaviour
{ 
    public GameObject terminalUI;
    public GameObject crosshair;

    public Text score1, score2, score3, score4, deaths1, deaths2, deaths3, deaths4;

        [SerializeField] private bool triggerActive = false;

        public void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                triggerActive = true;
            }
        }

        public void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                triggerActive = false;
            }
        }

        private void Update()
        {
            if (triggerActive && Input.GetKeyDown(KeyCode.E))
            {
                OpenTerminal();
            }
        }

        public void OpenTerminal()
        {
            terminalUI.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "GameScene")
        {

            score1.text = GameManager.Instance.skulls.ToString();
            deaths1.text = GameManager.Instance.deaths.ToString();
        }
        //GetComponent<PlayerInputHandler>().enabled = false;
        Cursor.visible = true;
        
    }

    public void exitButton()
    {
        terminalUI.SetActive(false);
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        //GetComponent<PlayerInputHandler>().enabled = true;
    }

    public void levelStage( int stage)
    {
        if (GameManager.Instance.Player.GetComponent<PlayerControl>().isHosting)
        {
            NetworkManager.Singleton.SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
            GameManager.Instance.OnSceneChanged();
        }
        terminalUI.SetActive(false);
    }

    public void levelFinishedToLobby()
    {
        GameManager.Instance.LevelComplete = true;
        NetworkManager.Singleton.SceneManager.LoadScene("LobbyScene", LoadSceneMode.Single);
        GameManager.Instance.OnSceneChanged();
        terminalUI.SetActive(false);
    }

}
