using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayMenuUI : MonoBehaviour
{
    [SerializeField]
    private Button startHostButton;
    [SerializeField]
    private Button startClientButton;
    [SerializeField]
    private Button menuButton;
  
    [SerializeField]
    private InputField joinCodeInput;

    private void Awake()
    {
        Cursor.visible = true;

    }

    private void Start()
    {
   
    }

    private void Update()
    {
        
    }
    public async void OnHostClickedAsync()
    {
        startHostButton.interactable = false;
        if (RelayManager.Instance.IsRelayEnabled)
        {
            await RelayManager.Instance.SetupRelay();
            //NetworkManager.Singleton.SceneManager.LoadScene("LobbyScene", LoadSceneMode.Single);
        }
        if (NetworkManager.Singleton.StartHost())
        {
            Debug.Log("Server requested");
        }
        
        
    }

    public async void OnClientClicked()
    {
        if (RelayManager.Instance.IsRelayEnabled && !string.IsNullOrEmpty(joinCodeInput.text))
        {   
            await RelayManager.Instance.JoinRelay(joinCodeInput.text);
            //NetworkManager.Singleton.SceneManager.LoadScene("LobbyScene", LoadSceneMode.Single);
        }       
        if (NetworkManager.Singleton.StartClient())
        {
            Debug.Log("Client requested");
        } 
    }

    public void ReturnButton()
    {
        //go back to Main menu
        SceneManager.LoadScene("MainMenu");
    }
}

