using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUIScript : MonoBehaviour
{
    bool toggle;
    // Start is called before the first frame update
    public void QuitButton()
    {
        Application.Quit();
    }

    // Update is called once per frame
    public void PlayButton()
    {
        SceneManager.LoadScene("PlayMenu");
    }

    public void SettingsButton()
    {
        SceneManager.LoadScene("SettingsScreen");
    }

    public void ArchivmentsButton()
    {
        SceneManager.LoadScene("AchievementsScreen");
    }
    public void ReturnButton()
    {
        //go back to Main menu
        SceneManager.LoadScene("MainMenu");
    }
    public void FullScreenButton()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }
    public void MuteGame()
    {
        toggle = !toggle;

        if (toggle)
            AudioListener.volume = 1f;

        else
            AudioListener.volume = 0f;
    }

    public void CreditsButton()
    {
        SceneManager.LoadScene("CreditsScene");
    }
}
