using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArchivementScript : MonoBehaviour
{
    public Image achivement1, achivement2, achivement3, achivement4, achivement5, achivement6;
    public Sprite achivement1U, achivement2U, achivement3U, achivement4U, achivement5U, achivement6U;
    // Start is called before the first frame update
    void Start()
    {
        if(GameManager.Instance.skulls>=10)
        achivement1.GetComponent<Image>().sprite = achivement1U;

        if (GameManager.Instance.LevelComplete = true)
        {
            achivement3.GetComponent<Image>().sprite = achivement1U;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
