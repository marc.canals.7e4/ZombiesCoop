using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class GameManager : NetworkBehaviour
{
    public GameObject Player, gameNetPortal, camera, networkManager;
    public GameObject ui;
    public int Hp;
    public bool usingConsumable;
    public float itemUse;
    public int skulls, deaths;
    public bool LevelComplete, isAlive = true;
    public int dmgBullet, pistolAmmo, shotgunAmmo, rifleAmmo, pistolMag, shotgunMag, rifleMag;
    public GameObject[] Players;
    public GameObject[] zombies;
    public PlayerInputHandler playerInput;
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public string GetSceneByName { get; private set; }

    private void Awake()
    {
        usingConsumable = false;
        itemUse = 0;
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        Hp = 100;
        pistolAmmo = 30;
        shotgunAmmo = 120;
        rifleAmmo = 64;
        pistolMag = 0;
        shotgunMag = 0;
        rifleMag = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Player != null && ui !=null && SceneManager.GetActiveScene().name == "GameScene") {
            if (Hp < 0) {
                Player.SetActive(false);
                Debug.LogWarning("Im dead");
                ui.GetComponent<LobbyUI>().deadMenu.SetActive(true);
                isAlive = false;
            }
        }

        if (GameObject.FindGameObjectsWithTag("Player") != null) {
            Players = GameObject.FindGameObjectsWithTag("Player");
            ui = GameObject.FindGameObjectWithTag("PlayerUI");
            for (int i = 0; i < Players.Length; i++)
            {
                if (Players[i].GetComponent<NetworkObject>().IsOwner)
                {
                    Player = Players[i];
                    if (ui != null && !Player.GetComponent<PlayerControl>().isHosting)
                    {
                        ui.GetComponent<LobbyUI>().activateHP(i);
                    }

                }
                
            }
        }
    }
    public void findPlayer()
    {

    }
    public void DmgPlayer(int dmg) {

    }

    public void OnSceneChanged() {
        playerInput.LoadScene();

        if (GetSceneByName == "LobbyScene")
        {
            for (int i = 0; i < Players.Length; i++) {
                Players[i].transform.position = Vector3.zero;}
                    
                    }
    }

    internal string ToString(string v)
    {
        throw new NotImplementedException();
    }
}
