using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class ObjectPool : NetworkBehaviour
{
    public GameObject objectToPool;

    public List<GameObject> pool = new List<GameObject>();

    public int amountObjects;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(fillPool());
           
    }
    public IEnumerator fillPool()
    {
        yield return new WaitForSeconds(1f);
        if (GameManager.Instance.Player.GetComponent<NetworkObject>().OwnerClientId == 0)
        {
            for (int i = 0; i < amountObjects; i++)
            {
                pool.Add(Instantiate(objectToPool));
                if (!pool[i].GetComponent<NetworkObject>().IsSpawned)
                {
                    pool[i].GetComponent<NetworkObject>().Spawn();
                    pool[i].GetComponent<NetworkObject>().RemoveOwnership();
                    
                }
                pool[i].SetActive(false);
                //pool[i].transform.parent = transform;
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.name.Equals("ZombiePool"))
        {
            GameManager.Instance.zombies = GameObject.FindGameObjectsWithTag("Zombie");
        }
    }

    public GameObject SpawnObject(Vector3 pos, Quaternion rot) { 
        GameObject returnObj;
        if (pool.Count > 0)
        {
            returnObj = pool[0];
            pool.RemoveAt(0);
        }
        else {
            returnObj = Instantiate(objectToPool);
            returnObj.transform.parent = transform;
        }
        returnObj.SetActive(true);
        returnObj.transform.position = pos;
        returnObj.transform.rotation = rot;

        return returnObj;
    }

    public void ReturnObject(GameObject returnObj) {
        pool.Add(returnObj);
        returnObj.SetActive(false);
    }
}
