using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(DestroyMyself());
    }

    public IEnumerator DestroyMyself()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(this.gameObject);
    }
    void Update()
    {
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Map"))
        {
            Destroy(this.gameObject);
        }

        if (collision.gameObject.CompareTag("Zombie"))
        {
            Destroy(this.gameObject);
        }
    }
}
